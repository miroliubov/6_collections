package src.services;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

public class StringServiceWithStream {

    public Map<String, Long> repeatWithStream(String text) {
        List<String> stringArray = Arrays.asList(text.split("[^a-zA-Zа-яА-Я0-9]+"));
        return stringArray.stream().collect(Collectors.groupingBy(Function.identity(), counting()));
    }

    public Set<String> uniqueWordWithStream(String text) {
        List<String> stringArray = Arrays.asList(text.split("[^a-zA-Zа-яА-Я0-9]+"));
        return stringArray.stream().collect(Collectors.toSet());

    }

    public List<String> sortedWordsWithStream(String text) {
        List<String> stringArray = Arrays.asList(text.split("[^a-zA-Zа-яА-Я0-9]+"));
        return stringArray.stream().distinct().sorted().collect(Collectors.toList());
    }
}
