package src.demo;

import src.services.StringService;
import src.services.StringServiceWithStream;

public class DemoService implements IDemoService {

    @Override
    public void execute() {
        String text = "And then, Clarisse was gone. He didn’t know what there was about the afternoon, but it was not " +
                "seeing her somewhere in the world. The lawn was empty, the trees empty, the street empty, and while " +
                "at first he did not even know he missed her or was even looking for her, the fact was that by the " +
                "time he reached the subway, there were vague stirrings of unease in him. Something was the matter, " +
                "his routine had been disturbed. A simple routine, true, established in a short few days, and yet…? " +
                "He almost turned back to make the walk again, to give her time to appear. He was certain if he tried " +
                "the same route, everything would work out fine. But it was late, and the arrival of his train put a " +
                "stop to his plan. The flutter of cards, motion of hands, of eyelids, the drone of the time-voice " +
                "in the firehouse ceiling “…one thirty-five. Thursday morning, November 4th… one thirty-six… one " +
                "thirty-seven a.m…? The tick of the playing-cards on the greasy table-top, all the sounds came to " +
                "Montag, behind his closed eyes, behind the barrier he had momentarily erected. He could feel the " +
                "firehouse full of glitter and shine and silence, of brass colours, the colours of coins, of gold, " +
                "of silver: The unseen men across the table were sighing on their cards, waiting. Montag looked at " +
                "these men whose faces were sunburnt by a thousand real and ten thousand imaginary fires, whose work " +
                "flushed their cheeks and fevered their eyes. These men who looked steadily into their platinum " +
                "igniter flames as they lit their eternally burning black pipes. They and their charcoal hair and " +
                "soot-coloured brows and bluish-ash-smeared cheeks where they had shaven close; but their heritage " +
                "showed. Montag started up, his mouth opened. Had he ever seen a fireman that didn’t have black hair, " +
                "black brows, a fiery face, and a blue-steel shaved but unshaved look? These men were all " +
                "mirror-images of himself! Were all firemen picked then for their looks as well as their " +
                "proclivities? The colour of cinders and ash about them, and the continual smell of burning from " +
                "their pipes. Captain Beatty there, rising in thunderheads of tobacco smoke. Beatty opening a fresh " +
                "tobacco packet, crumpling the cellophane into a sound of fire.";

        StringService repWord = new StringService();
        StringServiceWithStream stream = new StringServiceWithStream();

        System.out.println("Задание 1, количество каждого слова");
        System.out.println(repWord.repeatWord(text));

        System.out.println("\nЗадание 1, количество каждого слова, используя стримы");
        System.out.println(stream.repeatWithStream(text));

        System.out.println("\nЗадание 2, уникальные слова");
        System.out.println(repWord.uniqueWord(text));

        System.out.println("\nЗадание 2, уникальные слова, используя стримы");
        System.out.println(stream.uniqueWordWithStream(text));

        System.out.println("\nЗадание 3, сортировка слов, сортируется без учета регистра");
        System.out.println(repWord.sortedWords(text));

        System.out.println("\nЗадание 3, сортировка слов, используя стримы, сортируется с учетом регистра");
        System.out.println(stream.sortedWordsWithStream(text));
    }
}
